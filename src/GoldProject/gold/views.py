from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.utils import timezone 

def index(request):
		temp = loader.get_template('gold/index.html')
		con = {}
		return HttpResponse(temp.render(con, request))
