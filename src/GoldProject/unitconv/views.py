from django.shortcuts import render
from django.http import HttpResponse 
from django.http import HttpResponseRedirect 
from django.utils import timezone
from django.urls import reverse 
from django.template import loader
import json
from .models import convertUnits

def number(s):
	for c in s:
		if ((c < '0' or c > '0') and (c != '.')): return False
	return True

def convert(request):
	try:
		number = float(request.GET['value'])
		con = convertUnits
		t_oz = number * con.TtoP
		response = {"units": "t_oz", "value": t_oz}
		return HttpResponse(json.dumps(response))
	except ValueError:
		response = {"error": "Invalid unit conversion request" + str(type(request.GET['value']))}
		return HttpResonse(json.dumps(response))

def init(request):
	conversion.objects.all().delete()
	conv = conversion()
	print(conv.TtoP)
	conv.save()
	return HttpResonseRedirect(reverse('index', args = ()))